﻿using Npgsql;
using Otus_DB_Avito.Data;
using Otus_DB_Avito.Models;

namespace postgres
{
    class Program
    {
        const string ConnectionString = "User ID=postgres;Password=123;Host=localhost;Port=5432;Database=postgres;Connection Lifetime=0;";

        static void Main(string[] args)
        {
            var newUser = new User()
            {
                UserName = "Test",
                Date = DateTime.UtcNow,
                Phone = "88002993001"
            };

            InsertUser(newUser);

            ShowData();

            Console.ReadKey();
        }

        static void ShowData()
        {
            using AvitoDbContext ctxUser = new AvitoDbContext();
            var users = ctxUser.Users.ToList();
            foreach (var user in users)
            {
                Console.WriteLine(user.ToString());
            }

            using AvitoDbContext ctxPoster = new AvitoDbContext();
            var posteres = ctxPoster.Posteres.ToList();
            foreach (var poster in posteres)
            {
                Console.WriteLine(poster.ToString());
            }

            using AvitoDbContext ctxBasket = new AvitoDbContext();
            var baskets = ctxBasket.Baskets.ToList();
            foreach (var basket in baskets)
            {
                Console.WriteLine(basket.ToString());
            }
        }


        static void InsertUser(User user)
        {
            using AvitoDbContext ctx = new AvitoDbContext();
            ctx.Users.Add(user);
            ctx.SaveChanges();
        }

        static void GetVersion()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);
            connection.Open();
            var sql = "SELECT * FROM users";
            using var cmd = new NpgsqlCommand(sql, connection);
            var version = cmd.ExecuteReader().ToString();
            Console.WriteLine(version);
        }
    }
}

