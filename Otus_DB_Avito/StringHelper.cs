﻿namespace Otus_DB_Avito
{
    public class StringHelper
    {
        public static string Beautify(params object[] text)
        {
            var spacer = "                    ";
            var result = "";
            foreach (var item in text)
            {
                result += string.Concat(item.ToString(), spacer)[..20];
            }
            return result;
        }

        public string Beautify(string text)
        {
            var spacer = "                    ";
            return string.Concat(text, spacer)[..20];
        }
    }
}
