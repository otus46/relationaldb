﻿using Microsoft.EntityFrameworkCore;
using Otus_DB_Avito.Models;

namespace Otus_DB_Avito.Data
{
    public class AvitoDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Poster> Posteres { get; set; }
        public DbSet<Basket>  Baskets { get; set; }

        public AvitoDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("User ID=postgres;Password=123;Host=localhost;Port=5432;Database=postgres;Connection Lifetime=0;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
