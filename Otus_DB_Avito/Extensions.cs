﻿namespace Otus_DB_Avito
{
    public static class StringExtensions
    {
        public static string Beautify(this string text)
        {
            var spacer = "                    ";
            return string.Concat(text, spacer)[..20];
        }
    }
}
