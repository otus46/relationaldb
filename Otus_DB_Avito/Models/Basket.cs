﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Otus_DB_Avito.Models
{
    [Table("baskets")]
    public class Basket
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public long Posters_id { get; set; }
        public long User_id { get; set; }

        public override string ToString()
        {
            return StringHelper.Beautify(Id, Date, Posters_id, User_id);
        }
    }
    
}
