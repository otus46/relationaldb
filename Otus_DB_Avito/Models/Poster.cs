﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Otus_DB_Avito.Models
{
    [Table("posteres")]
    public class Poster
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }

        [Column("price")]
        public decimal Price { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        public override string ToString()
        {
            return StringHelper.Beautify(Id, Description, Date, Price, UserId);
        }
    }
}
