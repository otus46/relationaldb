﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Otus_DB_Avito.Models
{
    [Table("users")]
    public class User
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("username")]
        public string UserName { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        public override string ToString()
        {
            return StringHelper.Beautify(Id, UserName, Phone, Date);
        }
    
    }

}


