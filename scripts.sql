CREATE TABLE IF NOT EXISTS public.users
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    username "char" NOT NULL,
    date date NOT NULL,
    phone "char",
    CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.posteres
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    description "char",
    date date NOT NULL,
    price money,
    user_id bigint NOT NULL,
    CONSTRAINT posteres_pkey PRIMARY KEY (id),
    CONSTRAINT posteres_users FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE IF NOT EXISTS public.baskets
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Date" date NOT NULL,
    "Posters_id" bigint NOT NULL,
    "User_id" bigint NOT NULL,
    CONSTRAINT baskets_pkey PRIMARY KEY ("Id"),
    CONSTRAINT basket_posteres FOREIGN KEY ("Posters_id")
        REFERENCES public.posteres (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT basket_users FOREIGN KEY ("User_id")
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);

ЗАПОЛНЕНИЕ

INSERT INTO public.users(
	username, date, phone)
	VALUES ( 'fff', '2020-01-01', '123123123'),
	( 'test', '2021-05-05','2345235'),
	( 'Pasha', '2004-12-16', '12345123'),
	( 'Vadim', '2002-11-11', '165425123'),
	( 'Sasha', '2022-02-21', '11654');

INSERT INTO public.posteres(
	description, date, price, user_id)
	VALUES ( 'TEST', '1209-07-22', 42, 2),
	( 'tesla', '2005-11-24', 4200, 2),
	( 'shelv', '1996-03-19', 55, 2),
	( 'prosto', '1989-03-12', 4, 2),
	( 'home', '1999-04-23', 1442, 2);

INSERT INTO public.baskets(
	 "Date", "Posters_id", "User_id")
	VALUES ('2002-02-22',  8, 7),
	('2002-05-21',  8, 7),
	('2032-07-17', 8, 7),
	('2003-09-16',8, 7),
	('2022-12-15',8, 7);

User ID=postgres;Password=vaddik;Host=localhost;Port=5432;Database=postgres;Pooling=true;Min Pool Size=0;Max Pool Size=100;Connection Lifetime=0;